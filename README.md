# orderMealSystem

毕设点餐系统

## 项目介绍

包含服务器后端，后台管理页面和用户点餐页面。用户通过扫描餐桌上的二维码打开点餐页面，完成点餐、支付操作，后台管理界面显示新订单、处理订单记录等。后端采用 springboot、 mybatis、 mysql、websocket、消息队列等；前端采用 vue、 vant ui、 element ui等框架技术.

## 页面展示

### 点餐页面：

|                                                              |                                                              |                                                              |
| :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| <img src="https://s2.ax1x.com/2020/02/06/1yFppq.md.png" alt="image" style="zoom: 33%;" /> | <img src="https://s2.ax1x.com/2020/02/06/1yizhn.md.png" alt="image" style="zoom: 33%;" /> | <img src="https://s2.ax1x.com/2020/02/06/1yFCcV.md.png" alt="image" style="zoom: 33%;" /> |

| <img src="https://s2.ax1x.com/2020/02/06/1yixts.md.png" alt="image" style="zoom:50%;" /> | <img src="https://s2.ax1x.com/2020/02/06/1yivkj.md.png" alt="image" style="zoom:50%;" /> |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
|                                                              |                                                              |
| <img src="https://s2.ax1x.com/2020/02/06/1yF910.md.png" alt="image" style="zoom:50%;" /> | <img src="https://s2.ax1x.com/2020/02/06/1yFPXT.md.png" alt="image" style="zoom:50%;" /> |

